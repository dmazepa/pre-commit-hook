# Pre Commit Hook

Instruction how to set-up Pre Commit hook on your locale machine

1. Copy script from "helm/templates/pre-commit" to the folder ".git/hook" on your locale machine
2. To enable/disable Gitleaks verifiction during commit set git config using command: "git config hook.gitleaks.enable true/false"
2. Now during commit of your changes - you should get executed script which automatically set up gitleaks for your OS and ARCH and verify repository for leaks

NOTE!!!
1. On Windows OS it will work only using Git Bash application.
2. On LINUX execute command: "chmod +x .git/hooks/pre-commit" to allow  hook pre-commit to be executable